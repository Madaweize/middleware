<?php


namespace App;


use Interop\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Class Dispatcher
 * @package App
 */
class Dispatcher implements RequestHandlerInterface
{
    private $middleware;
    private  $index=0;

    /**
     * Permet d'enegister un nouveau middleware
     * @param callable|MiddlewareInterface $middleware
     */
    public function pipe($middleware){
        $this->middleware[]=$middleware;

    }

    /**
     * Permet d'executer un middleware
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request,ResponseInterface $response) :ResponseInterface{
        $middleware=$this->getMiddleware();
        $this->index++;
        if(is_null($middleware)){
            return $response;
        }

        if(is_callable($middleware)){
            return $middleware($request,$response, [$this,'process']);
        }elseif($middleware instanceof MiddlewareInterface){
            return $middleware->process($request,$this);
        }

    }

    private function getMiddleware(){
        if(isset($this->middleware[$this->index])){
                return $this->middleware[$this->index];
        }

        return null;
    }

    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // TODO: Implement handle() method.
    }
}