<?php

require 'vendor/autoload.php';

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use function Http\Response\send;


$trailingSlash=function (ServerRequestInterface $request, ResponseInterface $response, callable  $next){
    $url=(string)$request->getUri();
    if(!empty($url) &&  $url[-1]==='/'){
        $response=new \GuzzleHttp\Psr7\Response();
        return $response
                    ->withHeader('Location',substr($url,0,-1))
                    ->withStatus(301);
    }

    return $next($request,$response);
};

$quoteMidleware=function (ServerRequestInterface $request, ResponseInterface $response,callable $next){
    $response->getBody()->write('"');
    $response=$next($request,$response);
    $response->getBody()->write('"');

    return $response;

};

$app=function (ServerRequestInterface $request, ResponseInterface $response,callable $next){

    $url=$request->getUri()->getPath();

    if($url==='/blog'){
        $response->getBody()->write('Bienvenu sur mon blog');
    }else if ($url==='/contact'){
        $response->getBody()->write('Je suis sur contact');
    }else{
        $response->getBody()->write('not found');
        $response = $response->withStatus(404);
    }

    return $response;
};

//Va permettre de creer une requete a partir des variables globals($_POST,$_GET,$_SESSION...)
$request=\GuzzleHttp\Psr7\ServerRequest::fromGlobals();
$response=new \GuzzleHttp\Psr7\Response();

$dispatcher=new \App\Dispatcher();
$dispatcher->pipe(new \App\PowerdByMiddleWare());
$dispatcher->pipe($trailingSlash);
$dispatcher->pipe(new \Psr7Middlewares\Middleware\FormatNegotiator());
$dispatcher->pipe($quoteMidleware);
$dispatcher->pipe($app);

send($dispatcher->process($request,$response));

/*
$response= new \GuzzleHttp\Psr7\Response();

$url=$request->getUri()->getPath();


if($url==='/blog'){
    $response->getBody()->write('Bienvenu sur mon blog');
}else if ($url==='/contact'){
    $response->getBody()->write('Je suis sur contact');
}else{
    $response->getBody()->write('not found');
    $response = $response->withStatus(404);
}

use function Http\Response\send;

send($response);
*/
